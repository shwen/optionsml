import numpy as N
import sklearn.preprocessing as SkP

import tensorflow as tf

class PreprocessorHeston:
    ## NNHeston Preprocessor
    
    def __init__(self):
        self.scalerX = SkP.StandardScaler()
        self.scalerY = SkP.StandardScaler()
        
        self.ub = N.array([0.04,-0.1,1.0,0.2,10.0])
        self.lb = N.array([0.0001,-0.95,0.01,0.01,1])
        
    def fit(self, trainX, trainY):
        self.scalerY.fit(trainY.reshape(trainY.shape[0], -1))
        self.scalerX.fit(trainX)
        
    def transform_x(self, x):
        x = self.scalerX.transform(x)
        x = (2*x - (self.lb + self.ub)) / (self.ub - self.lb)
        return x
    
    def inverse_transform_x(self, x):
        x = x * (self.ub - self.lb) * 0.5 + (self.lb + self.ub) * 0.5
        x = self.scalerX.inverse_transform(x)
        return x
    
    def transform_y(self, y):
        shape = y.shape
        y = y.reshape(y.shape[0], -1)
        return self.scalerY.transform(y).reshape(shape)
    def inverse_transform_y(self, y):
        shape = y.shape
        y = y.reshape(y.shape[0], -1)
        return self.scalerY.inverse_transform(y).reshape(shape)
        
        
def loss_l0(y_true, y_pred):
    # MSE
    t1 = tf.sqrt(tf.reduce_mean((y_true - y_pred) ** 2, axis=[1,2]))
    # MAPE
    t2 = tf.reduce_mean(tf.abs(y_true - y_pred) / tf.abs(y_true), axis=[1,2])
    return tf.reduce_mean(t1 + t2)
