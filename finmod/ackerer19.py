import numpy as N
import numpy.random as NR
import numpy.linalg as NL
import scipy.stats as ScS

def call_option_price(s, sigma, r, delta, strike, dT):
    """
    Solving Black-Scholes PDE with boundary condition $V_T = (S_T + K)^+$.
    
    This is given in Ackerer19, Section 2.1 (3)
    """
    dcore = (N.log(s/strike) + (r - delta) * dT) / (sigma * N.sqrt(dT))
    dpart = 0.5 * sigma * N.sqrt(dT)
    return N.power(s, -delta * dT) * ScS.norm.cdf(dcore + dpart) \
        - N.exp(-r * dT) * strike * ScS.norm.cdf(dcore - dpart)

